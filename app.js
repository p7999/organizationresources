const express = require('express');
const bodyParser = require('body-parser');
const iecRoute = require('./routes/iec/iecRoute');
const waterRoute = require('./routes/water/waterRoute');
const policeRoute = require('./routes/police/policeRoute');

const app = express();
const port = 8080;
app.use(bodyParser.json());

iecRoute(app);
waterRoute(app);
policeRoute(app);

app.listen(port, () => {
  console.log(`PayGov server listening on port ${port}`);
});
