const { generateWaterPayments } = require('../../modules/water/water');

module.exports = (app) => {
    app.get('/water/:userId', (req, res) => {
      console.log(
        'Water payment for user: ' + req.params.userId
      );
      let payments = generateWaterPayments(req.params.userId)
      res.send(payments);
    });
  };