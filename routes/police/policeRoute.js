const { generatePolicePayments } = require('../../modules/police/police');

module.exports = (app) => {
    app.get('/police/:userId', (req, res) => {
      console.log(
        'Police payment for user: ' + req.params.userId
      );
      let payments = generatePolicePayments(req.params.userId)
      res.send(payments);
    });
  };