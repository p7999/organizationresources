const { generateIECPayments } = require('../../modules/iec/iec');

module.exports = (app) => {
    app.get('/iec/:userId', (req, res) => {
      console.log(
        'IEC payment for user: ' + req.params.userId
      );
      let payments = generateIECPayments(req.params.userId)
      res.send(payments);
    });
  };