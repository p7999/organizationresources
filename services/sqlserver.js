const sql = require('mssql')
const _ = require('lodash')

class Sqlserver{
    constructor(sqlConfig) {
        this.sqlConfig = sqlConfig;
    }

    async selectQuery(query){
        try {
            await sql.connect(this.sqlConfig);
            const result = await sql.query(query);
            const res = _.get(result,['recordset']);
            return res;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
    async dmlRequest(dml){
        try {
            await sql.connect(this.sqlConfig);
            const result = await sql.query(dml);
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}
module.exports = {
    Sqlserver
}