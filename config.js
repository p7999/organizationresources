module.exports = {
    sql:{
        user: process.env.DB_USER || 'paygov',
        password: process.env.DB_PWD || 'paygov',
        database: process.env.DB_NAME || 'paygov',
        server: process.env.SERVER_HOST,
        pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 30000
        },
        options: {
            encrypt: false, // for azure
            trustServerCertificate: false // change to true for local dev / self-signed certs
        }
    }
}