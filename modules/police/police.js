const billData =  [
    {
        billTotal: 750,
        desc: "דוח מהירות",
        speed: {
            value: Math.floor(Math.random() * 220) + 125,
            description: "מהירות"
        }
    }, 
    {
        billTotal: 1100,
        desc: "מעבר באור אדום",
        location: {
            value: Math.floor(Math.random() * 4) + 1,
            description: "מיקום המקרה"
        }
    }, 
    {
        billTotal: 500,
        desc: "חנייה בחניית נכים",
        location: {
            value: Math.floor(Math.random() * 4) + 1,
            description: "מיקום המקרה"
        }
    }
];

const generatePolicePayments = (user_id) => {
    let numOfPayments = Math.floor(Math.random() * 3) + 1;
    let payments = [];
    let date = new Date();
    for (let index = 0; index < numOfPayments; index++) {
        let paymentType = Math.floor(Math.random() * 3); 
        let startDate = date.getTime();
        date.setDate(date.getDate() + 30);
        let endDate = date.getTime();
        let payment = {
            "personal_id": user_id,
            "bill_end_date": endDate,
            "bill_start_date": startDate,
            "bill_total": billData[paymentType].billTotal,
            "description": billData[paymentType].desc,
            "additional": billData[paymentType].speed ?
            billData[paymentType].speed : billData[paymentType].location
        };

        payments.push(payment);
        date = new Date();
        let months = Math.floor(Math.random() * 6) + 2;
        date.setDate(date.getDate() - 30 * months);
    }

    return payments;
};
  
module.exports = { generatePolicePayments };