const billPrices = [300, 485, 224];

const generateIECPayments = (user_id) => {
    let numOfPayments = Math.floor(Math.random() * 3) + 1;
    let payments = [];
    let date = new Date();
    for (let index = 0; index < numOfPayments; index++) {
        let paymentPrice = billPrices[Math.floor(Math.random() * 3)];
        let startDate = date.getTime();
        date.setDate(date.getDate() + 30);
        let endDate = date.getTime();
        let payment = {
            "personal_id": user_id,
            "bill_end_date": endDate,
            "bill_start_date": startDate,
            "bill_total": paymentPrice,
            "additional": {
                "bill_total_kWh": {
                    "value": paymentPrice / 1.75,
                    "description": "כמות וואט לתשלום"
                }
            }
        };

        payments.push(payment);
        date = new Date();
        let months = Math.floor(Math.random() * 6) + 2;
        date.setDate(date.getDate() - 30 * months);
    }

    return payments;
};
  
module.exports = { generateIECPayments };